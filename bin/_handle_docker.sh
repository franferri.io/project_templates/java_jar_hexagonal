#!/bin/bash

source "$(realpath "${BASH_SOURCE%/*}/libs/env")"
source "${ORIGINAL_SCRIPT_PATH}/libs/debug"
source "${ORIGINAL_SCRIPT_PATH}/libs/functions"

set -x

cd "${ORIGINAL_SCRIPT_PATH}" || exit 1
cd .. || exit 1

# git ls-files -v | grep '^h' | cut -c 3- | xargs git update-index --no-assume-unchanged
git update-index --assume-unchanged .gitignore
git update-index --assume-unchanged boot/src/main/resources/application-local.yml
# git update-index --assume-unchanged pom.xml

if ! docker images postgres:14.10-alpine | grep -q postgres; then
    docker pull postgres:14.10-alpine
fi
docker stop contrans || true
docker rm contrans || true
docker run --name contrans -e initdb -e POSTGRES_PASSWORD=postgres -p 5432:5432 -d postgres:14.10-alpine

container_name="contrans"
echo "Waiting for PostgreSQL to start..."
while ! docker exec $container_name pg_isready -U postgres -h localhost > /dev/null 2>&1; do
    echo "PostgreSQL is not yet ready. Retrying in 5 seconds..."
    sleep 1
done
echo "PostgreSQL is now ready!"

docker exec -i contrans psql -U postgres <<EOF
CREATE DATABASE "contrans-main";
\c "contrans-main";
CREATE SCHEMA "adm-contrans-main";
EOF

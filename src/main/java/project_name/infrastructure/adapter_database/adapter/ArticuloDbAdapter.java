package project_name.infrastructure.adapter_database.adapter;

import project_name.domain.model.ArticuloDOMAIN;
import project_name.domain.ports.infrastructure.DatabasePort;
import project_name.infrastructure.adapter_database.mapper.ArticulosMAPPER;
import project_name.infrastructure.adapter_database.model.ArticuloMO;
import project_name.infrastructure.adapter_database.repositories.ArticulosREPOSITORY;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ArticuloDbAdapter implements DatabasePort {

    private final ArticulosREPOSITORY databaseRepository;

    private final ArticulosMAPPER mapper = ArticulosMAPPER.INSTANCE;

    @Override
    public ArticuloDOMAIN getArticuloByName(String name) {

        System.out.println("Llamada recibida ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS al método getArticulo, con valor: " + name);

        System.out.println("El ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS crea un Artículo de ejemplo con el name indicado por el usuario");
        ArticuloMO clinex = new ArticuloMO(1234L, name, "00012344321", "Paquete de clinex");
        databaseRepository.save(clinex);

        ArticuloMO articuloMO = databaseRepository.getArticulo(name);

        System.out.println("El ADAPTADOR DE INFRAESTRUCTURA DE BASE DE DATOS devuelve los datos recibidos");

        return mapper.aDominio(articuloMO);

    }
}

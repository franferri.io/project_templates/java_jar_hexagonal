package project_name;

import com.tngtech.archunit.core.importer.ImportOption;
import com.tngtech.archunit.junit.AnalyzeClasses;
import com.tngtech.archunit.junit.ArchTest;
import com.tngtech.archunit.lang.ArchRule;

import static com.tngtech.archunit.lang.syntax.ArchRuleDefinition.noClasses;

@AnalyzeClasses(packages = "project_name", importOptions = ImportOption.DoNotIncludeTests.class)
public class HexagonalIntegrityTests {

    private static String APPLICATION = "..project_name.application.";
    private static String APPLICATION_ADAPTER_REST = APPLICATION + "adapter_rest..";
    private static String DOMAIN = "..project_name.domain..";
    private static String INFRASTRUCTURE = "..project_name.infrastructure.";
    private static String INFRASTRUCTURE_ADAPTER_DATABASE = INFRASTRUCTURE + "adapter_database..";

    @ArchTest
    public static final ArchRule APPLICATION_ADAPTER_REST_PACKAGES =
        noClasses().that().resideOutsideOfPackage(APPLICATION_ADAPTER_REST)
            .should().dependOnClassesThat().resideInAnyPackage(APPLICATION_ADAPTER_REST);

    @ArchTest
    public static final ArchRule DOMAIN_PACKAGES =
        noClasses().that().resideInAPackage(DOMAIN)
            .should().dependOnClassesThat().resideInAnyPackage(APPLICATION, INFRASTRUCTURE);

    @ArchTest
    public static final ArchRule INFRASTRUCTURE_ADAPTER_DATABASE_PACKAGES =
        noClasses().that().resideOutsideOfPackage(INFRASTRUCTURE_ADAPTER_DATABASE)
            .should().dependOnClassesThat().resideInAnyPackage(INFRASTRUCTURE_ADAPTER_DATABASE);

}
